# WebhookOutputBodyDataRestEndPoints

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **bool** | If webhook is enabled or not | 
**authKey** | **string** | Authorization key for validating braango webhook requests | 
**authId** | **string** | ID for this webhook | 
**postUrl** | **string** | URL where webhook data can be POSTed | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


