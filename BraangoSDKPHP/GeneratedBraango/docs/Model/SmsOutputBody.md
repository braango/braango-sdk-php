# SmsOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | SUCCESS indicates the operation is successful. If ERROR , then message and message-code will further indicate errors | 
**message** | **string** | Optional, present only in case of ERROR status | [optional] 
**messageCode** | **string** | Optional, present only in case of ERROR status and further indicates the ERROR code | [optional] 
**data** | [**\Braango\braangomodel\SmsOutputBodyData**](SmsOutputBodyData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


