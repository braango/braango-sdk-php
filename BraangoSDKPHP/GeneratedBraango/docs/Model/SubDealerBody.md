# SubDealerBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsLogin** | **bool** | Indicates whether this personnel can login via SMS. SMS enabled number can act as user id and 6 digit validation code that personnel received can be used in lieu of password. For this to happen, sms_login needs to be enabled. | [optional] [default to false]
**dealerName** | **string** | Name assigned while signing up this personnel. For _partner dealer_ (master account) this is assigned while creating an account | [optional] 
**personnelName** | **string** | Name assigned to this personnel | 
**userName** | **string** | This is a user name created while signing this personnel up. Typically this user name can be used to log into the braango UI. However for whitelabel product, it is expected that this will be used for single signon with respect to dealer account on partner system. i.e. it is expected that partner will pass on the same user name that dealer has on its system to give seamless integration experience. | 
**password** | **string** | Password will be encrypted with SHA-25 and base64 encoded and stored internally within the braango system. | 
**status** | **string** | Values are Active, Inactive, Vacation | [optional] 
**enabled** | **bool** | For account that is active, this will be true. | [optional] 
**dealerBanners** | **string[]** | List of banners that this personnel will see when client sends TEXT. Braango will randomly pick one from the list | [optional] 
**clientBanners** | **string[]** | Banner message that client will see when this personnel communications over SMS. Braango will randomly choose one from the list | [optional] 
**supervisorBanners** | **string[]** | When personnel is acting as supervisor, this will be the banner message that personnel will see. Braango will choose one from the list randomly. | [optional] 
**clientFooters** | **string[]** | Footer message that client sees when this personnel communicates over SMS. Braango will randomly choose from one | [optional] 
**dealerFooters** | **string[]** | Footer message that this personnel will see when client communicates. Braango will randomly choose one from this list. | [optional] 
**supervisorFooters** | **string[]** | Footer message that this personnel will see  when acting as supervisor when client communicates. Braango will randomly choose one from this list. | [optional] 
**email** | **string** | Email id of personnel | [optional] 
**smsNumber** | **string** | Number where dealer&#39;s root account can be reached via SMS | [optional] 
**phoneNumber** | **string** |  | [optional] 
**group** | **string** | Groups this subDealer is subscribed/owner of | [optional] 
**typeAdfCrm** | **bool** | Email being specified is that for ADF XML based CRM | [optional] [default to false]
**package** | **string** |  | 
**subDealerId** | **string** | Sub Dealer ID | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


