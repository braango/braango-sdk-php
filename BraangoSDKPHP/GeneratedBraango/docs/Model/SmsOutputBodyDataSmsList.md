# SmsOutputBodyDataSmsList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **string** | Actual number | 
**carrier** | **string** | Carrier | 
**usage** | **string** | Usage | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


