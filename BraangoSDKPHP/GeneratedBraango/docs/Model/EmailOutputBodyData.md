# EmailOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**emailList** | **string[]** | List of emails personnel has subscribed to. Will be returned if request had type_adf_crm &#x3D; false, else will be null | [optional] 
**crmEmailList** | **string[]** | List of CRM emails personnel has subscribed to. Will be returned if request had type_adf_crm &#x3D; true, else will be null | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


