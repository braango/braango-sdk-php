# ErrorBodyConflict

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | Error due to resource conflict | 
**message** | **string** | Human readable message for conflict | 
**messageCode** | **string** | machine parsable code for conflict | 
**data** | **object** | Error payload | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


