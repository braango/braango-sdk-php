# FootersInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerFooters** | **string[]** | Footers that dealer will see. Braango will randomly choose one of these for every TEXT message received and insert it as footer | [optional] 
**clientFooters** | **string[]** | Footers that client will see. Braango will randomly choose one of these for every TEXT message received and insert it as footer | [optional] 
**supervisorFooters** | **string[]** | Footers that supervisor will see. Braango will randomly choose one of these for every TEXT message received and insert it as footer | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


