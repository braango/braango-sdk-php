# SupervisorOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | Indicates if request is SUCCES,ERROR or WARNING | 
**message** | **string** | Human readable ERROR or WARNING message | [optional] 
**messageCode** | **string** | Machine parse able ERROR or WARNING code | [optional] 
**data** | [**\Braango\braangomodel\SupervisorOutputBodyData**](SupervisorOutputBodyData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


