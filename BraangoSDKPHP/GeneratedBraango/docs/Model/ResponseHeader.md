# ResponseHeader

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isnRequestId** | **string** | ID generated for every request by braango system. All requests are logged into the braango database and this is the key for that entry | 
**id** | **string** | This is reflection of id of _request_header&#39;s_ id . | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


