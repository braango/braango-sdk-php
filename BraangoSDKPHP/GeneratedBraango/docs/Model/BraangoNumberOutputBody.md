# BraangoNumberOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** | More description of error message. This field is conditional and may not be present. | [optional] 
**messageCode** | **string** | This describes the error code. This field will be conditional | [optional] 
**data** | [**\Braango\braangomodel\BraangoNumberOutputBodyData**](BraangoNumberOutputBodyData.md) |  | [optional] 
**status** | **string** | Status of the API request call | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


