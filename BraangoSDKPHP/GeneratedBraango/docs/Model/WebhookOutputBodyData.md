# WebhookOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**restEndPoints** | [**\Braango\braangomodel\WebhookOutputBodyDataRestEndPoints[]**](WebhookOutputBodyDataRestEndPoints.md) | Array of REST endpoint objects | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


