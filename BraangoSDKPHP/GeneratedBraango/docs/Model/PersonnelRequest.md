# PersonnelRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsLogin** | **bool** | Indicates whether this personnel can login via SMS. SMS enabled number can act as user id and 6 digit validation code that personnel received can be used in lieu of password. For this to happen, sms_login needs to be enabled. | [optional] [default to false]
**personnelName** | **string** | Name assigned to this personnel | 
**userName** | **string** | This is a user name created while signing this personnel up. Typically this user name can be used to log into the braango UI. However for whitelabel product, it is expected that this will be used for single signon with respect to dealer account on partner system. i.e. it is expected that partner will pass on the same user name that dealer has on its system to give seamless integration experience. | 
**password** | **string** | Password will be encrypted with SHA-25 and base64 encoded and stored internally within the braango system. | 
**status** | **string** | Values are Active, Inactive, Vacation  Default value is \&quot;Active\&quot; if this field is not passed | [optional] 
**enabled** | **bool** | For account that is active, this will be true. | [optional] 
**dealerBanners** | **string[]** | List of banners that this personnel will see when client sends TEXT. Braango will randomly pick one from the list | [optional] 
**clientBanners** | **string[]** | Banner message that client will see when this personnel communications over SMS. Braango will randomly choose one from the list | [optional] 
**supervisorBanners** | **string[]** | When personnel is acting as supervisor, this will be the banner message that personnel will see. Braango will choose one from the list randomly. | [optional] 
**clientFooters** | **string[]** | Footer message that client sees when this personnel communicates over SMS. Braango will randomly choose from one | [optional] 
**dealerFooters** | **string[]** | Footer message that this personnel will see when client communicates. Braango will randomly choose one from this list. | [optional] 
**supervisorFooters** | **string[]** | Footer message that this personnel will see  when acting as supervisor when client communicates. Braango will randomly choose one from this list. | [optional] 
**email** | **string** | Email address where personnel can receive and send TEXT messages from | [optional] 
**smsNumber** | **string** | Cell number of personnel | [optional] 
**phoneNumber** | **string** | Number where personnel can receive voice calls | [optional] 
**group** | **string** | Group that personnel is attempting to subscribe to | [optional] 
**typeAdfCrm** | **bool** | If true, email specified is for ADF compatible CRM | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


