# BraangoNumberCreateInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**\Braango\braangomodel\RequestHeader**](RequestHeader.md) |  | 
**body** | [**\Braango\braangomodel\BraangoNumberCreateInputBody**](BraangoNumberCreateInputBody.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


