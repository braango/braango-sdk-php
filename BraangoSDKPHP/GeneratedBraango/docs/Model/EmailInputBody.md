# EmailInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Email where personnel can receive or send text messages from | 
**typeAdfCrm** | **bool** | If true, email represent ADF compatible CRM | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


