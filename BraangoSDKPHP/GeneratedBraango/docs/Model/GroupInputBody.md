# GroupInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **string** | Group that personnel is trying to subscribe or group that sub_dealer is trying to create | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


