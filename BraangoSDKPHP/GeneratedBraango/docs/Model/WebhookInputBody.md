# WebhookInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **bool** |  | 
**postUrl** | **string** | Post back URL. Braango will create POST request to this URL | 
**authKey** | **string** | API Key or token to use to validate requests. Braango will use this as part of header in its web-hook request. Please see &lt;&gt; for the web hook format | 
**authId** | **string** | ID identifying this resource | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


