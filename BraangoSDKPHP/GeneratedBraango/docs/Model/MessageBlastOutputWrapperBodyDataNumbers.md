# MessageBlastOutputWrapperBodyDataNumbers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phoneNumber** | **string** | phone_number that was sent message to | 
**numberId** | **string** | reflection of id for this number | 
**dateRequestSent** | **string** | Return will in this format MM/dd/yyyy:hh:mm:aa | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


