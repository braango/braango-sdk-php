# FootersOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | Determines if the operation was SUCCESS or ended with ERROR. If ERROR, message will have details and MESSAGE-CODE will have detailed error code | [default to 'SUCCESS']
**message** | **string** | Only valid if status is ERROR | [optional] 
**messageCode** | **string** | Only valid if status is ERROR. This will return the detailed ERROR code | [optional] 
**data** | [**\Braango\braangomodel\FootersOutputBodyData**](FootersOutputBodyData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


