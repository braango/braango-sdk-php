# AccountInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userName** | **string** |  | 
**password** | **string** |  | 
**firstName** | **string** |  | 
**lastName** | **string** |  | 
**email** | **string** |  | 
**businessName** | **string** |  | 
**cellPhone** | **string** | Required for non _virtual_dealer_ mode . For real dealer, this number will be validated for duplication within _braango_ system . If not duplicate, this will be seeded as number on which this real dealer can receive TEXT and VOICE messages | 
**package** | **string** | For _virtual_dealer_ mode, suggested package is \&quot;Franchise\&quot; if _virtual_dealer_ is channel partner | 
**zipCodes** | **string[]** | Only applicable for _real_dealer_ mode . For _virtual_dealer_ mode, entire US/Canada is the region | [optional] 
**raidus** | **float** | Only applicable for real dealer mode. -1.0 value indicates no restrictions. If +ve value, then zipCodes need to be speficied. Value is of double type | [optional] 
**virtualDealer** | **bool** | Indicates whether virtual dealer mode is to be turned on for this _partner_dealer_ . All __vritual__ _partner_dealer_ accounts will have ability to be originator of &#x60;braango leads&#x60; functionality. They can blast and connect leads for the _sub_dealers_ they manage.   Each _partner_dealer_ can have its own set of _sub_dealers_ .   To create &#x60;braango leads&#x60; blast number for virtual dealer, please contact sales@braango.com . Currently blast number for _partner_dealer_ have to be seeded manually  Default is true | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


