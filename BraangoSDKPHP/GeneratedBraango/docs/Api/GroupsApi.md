# Braango\GroupsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createGroup**](GroupsApi.md#createGroup) | **POST** /personnel/groups/{subdealerid}/{salespersonid} | Create Group
[**deleteAllGroups**](GroupsApi.md#deleteAllGroups) | **DELETE** /personnel/groups/{subdealerid}/{salespersonid} | Delete All Groups
[**deleteGroup**](GroupsApi.md#deleteGroup) | **DELETE** /personnel/groups/{subdealerid}/{salespersonid}/{group} | Delete Group
[**getGroup**](GroupsApi.md#getGroup) | **GET** /personnel/groups/{subdealerid}/{salespersonid} | Get Group


# **createGroup**
> \Braango\braangomodel\GroupOutput createGroup($subdealerid, $salespersonid, $body)

Create Group

Create or subscribe to a group. If the requesting personnel is _sub_dealer_, then a group is created, else personnel is subscribed to a given group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\GroupsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$body = new \Braango\braangomodel\GroupInput(); // \Braango\braangomodel\GroupInput | 

try {
    $result = $api_instance->createGroup($subdealerid, $salespersonid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->createGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **body** | [**\Braango\braangomodel\GroupInput**](../Model/GroupInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\GroupOutput**](../Model/GroupOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllGroups**
> \Braango\braangomodel\GroupOutput deleteAllGroups($subdealerid, $salespersonid, $apiKey, $accountType)

Delete All Groups

This call deletes all the groups associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for each of the group are removed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\GroupsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteAllGroups($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->deleteAllGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\GroupOutput**](../Model/GroupOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteGroup**
> \Braango\braangomodel\GroupOutput deleteGroup($subdealerid, $salespersonid, $group, $apiKey, $accountType)

Delete Group

This call deletes  the group specified in the call associated with this _personnel_ . __DEFAULT__ group can never be deleted  All the associations of _supervisors_ and _subordinates_ for the group are removed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\GroupsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$group = "group_example"; // string | Group being asked to be deleted
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteGroup($subdealerid, $salespersonid, $group, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->deleteGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **group** | **string**| Group being asked to be deleted |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\GroupOutput**](../Model/GroupOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getGroup**
> \Braango\braangomodel\GroupOutput getGroup($subdealerid, $salespersonid, $apiKey, $accountType)

Get Group

Get list of groups that this personnel has subscribed to.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\GroupsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getGroup($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GroupsApi->getGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\GroupOutput**](../Model/GroupOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

