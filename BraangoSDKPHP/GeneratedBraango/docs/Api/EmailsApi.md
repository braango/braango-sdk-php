# Braango\EmailsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createEmail**](EmailsApi.md#createEmail) | **POST** /personnel/email/{subdealerid}/{salespersonid} | Create email
[**deleteEmail**](EmailsApi.md#deleteEmail) | **DELETE** /personnel/email/{subdealerid}/{salespersonid} | Delete email
[**deleteOneEmail**](EmailsApi.md#deleteOneEmail) | **DELETE** /personnel/email/{subdealerid}/{salespersonid}/{email} | Delete One email
[**getEmail**](EmailsApi.md#getEmail) | **GET** /personnel/email/{subdealerid}/{salespersonid} | Get email


# **createEmail**
> \Braango\braangomodel\EmailOutput createEmail($subdealerid, $salespersonid, $body)

Create email

Add the email where personnel can receive and send TEXT messages from. If type_adf_crm is set to true, the email represents ADF XML compatible CRM email address

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\EmailsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | if of _personnel_
$body = new \Braango\braangomodel\EmailInput(); // \Braango\braangomodel\EmailInput | 

try {
    $result = $api_instance->createEmail($subdealerid, $salespersonid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmailsApi->createEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| if of _personnel_ |
 **body** | [**\Braango\braangomodel\EmailInput**](../Model/EmailInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\EmailOutput**](../Model/EmailOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteEmail**
> \Braango\braangomodel\EmailOutput deleteEmail($subdealerid, $salespersonid, $apiKey, $accountType, $typeAdfCrm)

Delete email

Delete all emails associated with this personnel.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\EmailsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | if of _personnel_
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API
$typeAdfCrm = true; // bool | Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts

try {
    $result = $api_instance->deleteEmail($subdealerid, $salespersonid, $apiKey, $accountType, $typeAdfCrm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmailsApi->deleteEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| if of _personnel_ |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]
 **typeAdfCrm** | **bool**| Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts | [optional]

### Return type

[**\Braango\braangomodel\EmailOutput**](../Model/EmailOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteOneEmail**
> \Braango\braangomodel\EmailOutput deleteOneEmail($subdealerid, $salespersonid, $email, $apiKey, $accountType, $typeAdfCrm)

Delete One email

Delete one particular email address as specified in the URL

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\EmailsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$email = "email_example"; // string | Email to be deleted
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API
$typeAdfCrm = true; // bool | Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts

try {
    $result = $api_instance->deleteOneEmail($subdealerid, $salespersonid, $email, $apiKey, $accountType, $typeAdfCrm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmailsApi->deleteOneEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **email** | **string**| Email to be deleted |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]
 **typeAdfCrm** | **bool**| Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts | [optional]

### Return type

[**\Braango\braangomodel\EmailOutput**](../Model/EmailOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEmail**
> \Braango\braangomodel\EmailOutput getEmail($subdealerid, $salespersonid, $apiKey, $accountType, $typeAdfCrm)

Get email

Get the list of emails asscociated with this personnel. Based on if type_adf_crm flag, list returned is of CRM emails or regular emails

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\EmailsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | if of _personnel_
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API
$typeAdfCrm = true; // bool | Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts

try {
    $result = $api_instance->getEmail($subdealerid, $salespersonid, $apiKey, $accountType, $typeAdfCrm);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmailsApi->getEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| if of _personnel_ |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]
 **typeAdfCrm** | **bool**| Specifies to get ADF CRM email records or plain email records. If not specified, default is false i.e. regular email ID accounts | [optional]

### Return type

[**\Braango\braangomodel\EmailOutput**](../Model/EmailOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

