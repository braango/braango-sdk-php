# Braango\SupervisorsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSupervisor**](SupervisorsApi.md#createSupervisor) | **POST** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Create Supervisor
[**deleteAllSupervisors**](SupervisorsApi.md#deleteAllSupervisors) | **DELETE** /personnel/supervisors/{subdealerid}/{salespersonid} | Delete All Supervisor
[**deleteSupervisor**](SupervisorsApi.md#deleteSupervisor) | **DELETE** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Delete Supervisor
[**getAllSupervisors**](SupervisorsApi.md#getAllSupervisors) | **GET** /personnel/supervisors/{subdealerid}/{salespersonid} | List Supervisors
[**getSupervisor**](SupervisorsApi.md#getSupervisor) | **GET** /personnel/supervisors/{subdealerid}/{salespersonid}/{group} | Get Supervisor


# **createSupervisor**
> \Braango\braangomodel\SupervisorOutput createSupervisor($subdealerid, $salespersonid, $group, $body)

Create Supervisor

Add a supervisor for given personnel, given group. Supervisor is another personnel that needs to exist in the system

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SupervisorsApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$group = "group_example"; // string | 
$body = new \Braango\braangomodel\SupervisorInput(); // \Braango\braangomodel\SupervisorInput | 

try {
    $result = $api_instance->createSupervisor($subdealerid, $salespersonid, $group, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupervisorsApi->createSupervisor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **group** | **string**|  |
 **body** | [**\Braango\braangomodel\SupervisorInput**](../Model/SupervisorInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\SupervisorOutput**](../Model/SupervisorOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllSupervisors**
> \Braango\braangomodel\SupervisorOutput deleteAllSupervisors($subdealerid, $salespersonid, $apiKey, $accountType)

Delete All Supervisor

Delete all supervisors for given personnel, all groups

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SupervisorsApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteAllSupervisors($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupervisorsApi->deleteAllSupervisors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\SupervisorOutput**](../Model/SupervisorOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSupervisor**
> \Braango\braangomodel\SupervisorOutput deleteSupervisor($subdealerid, $salespersonid, $group, $apiKey, $accountType)

Delete Supervisor

Delete supervisors for given personnel for given group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SupervisorsApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$group = "group_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteSupervisor($subdealerid, $salespersonid, $group, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupervisorsApi->deleteSupervisor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **group** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\SupervisorOutput**](../Model/SupervisorOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllSupervisors**
> \Braango\braangomodel\SupervisorOutput getAllSupervisors($subdealerid, $salespersonid, $apiKey, $accountType)

List Supervisors

Get all supervisors for all groups for given personnel

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SupervisorsApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getAllSupervisors($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupervisorsApi->getAllSupervisors: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\SupervisorOutput**](../Model/SupervisorOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSupervisor**
> \Braango\braangomodel\SupervisorOutput getSupervisor($subdealerid, $salespersonid, $group, $apiKey, $accountType)

Get Supervisor

Get list of supervisors for given personnel for given group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SupervisorsApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$group = "group_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getSupervisor($subdealerid, $salespersonid, $group, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupervisorsApi->getSupervisor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **group** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\SupervisorOutput**](../Model/SupervisorOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

