# Braango\AccountsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPartnerDealer**](AccountsApi.md#createPartnerDealer) | **POST** /accounts | Create Partner Dealer Account


# **createPartnerDealer**
> \Braango\braangomodel\AccountCreateOutputWrapper createPartnerDealer($body)

Create Partner Dealer Account

## Create Account   Use this call to create a **partner dealer** account into the braango system. Typically this could be a real dealership or it could be virtual dealer created by channel partner. The partner dealer account can hold more than 1 sub dealers based on package selected.   >sub dealers are entities that have privileges to add personnel, create supervisors, create groups etc.   >In virual dealer mode, typically sub dealer will be actual business the partner is registering with the braango system.  > In real dealer mode, sub dealer could be a location  > ###### Note sub dealer are actually personnel with full privileges. Typically businesses can use general manager personnel for sub dealer details  Typically based on channel partner's business model, either channel partner can create a virtual dealer and have all of his clients as sub dealers. This allows for single dashboad managent for partner's all dealers. Further more, this model allows tighter control over some global resources that channel partner may want to employ. To use this model, please select package \"Enterprise\". Currently for each virtual dealer, there can be 3000 sub dealers created.  Virtual dealer model is highly recommended for braango leads product. Further more virutal dealer model allows for flat hierarchy --> Virtual Dealer->Sub Dealer->Personnel.  For virtual dealer model, each franhcise location will be created as separate sub-dealer  For real dealer model i.e channel partner creating a real dealer account is good for scenarios where franchise dealers want their own hierarchy and single dashboard.  ----------------------------------------  -----------  Account creation process involves following steps  - Creating partnerDealer account into Braango System for generation of API key of this account - Creating default sub dealer for the partner dealer (internally created automatically) - Creating package for this partner dealer account (internally created automatically)  Note each partnerDealer account can have its own braango blast number. This has to be seeded manually at the moment, please contact support@braango.com if your use case needs that

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\AccountsApi();
$body = new \Braango\braangomodel\AccountCreateRequestInput(); // \Braango\braangomodel\AccountCreateRequestInput | 

try {
    $result = $api_instance->createPartnerDealer($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->createPartnerDealer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Braango\braangomodel\AccountCreateRequestInput**](../Model/AccountCreateRequestInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\AccountCreateOutputWrapper**](../Model/AccountCreateOutputWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

