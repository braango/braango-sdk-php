# Braango\VoiceApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVoice**](VoiceApi.md#createVoice) | **POST** /personnel/voice/{subdealerid}/{salespersonid} | Create Voice
[**deleteOneVoice**](VoiceApi.md#deleteOneVoice) | **DELETE** /personnel/voice/{subdealerid}/{salespersonid}/{number} | Delete One Voice
[**deleteVoice**](VoiceApi.md#deleteVoice) | **DELETE** /personnel/voice/{subdealerid}/{salespersonid} | Delete Voice
[**getVoice**](VoiceApi.md#getVoice) | **GET** /personnel/voice/{subdealerid}/{salespersonid} | Get Voice


# **createVoice**
> \Braango\braangomodel\VoiceOutput createVoice($subdealerid, $salespersonid, $body)

Create Voice

Add voice number for the given personnel

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\VoiceApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$body = new \Braango\braangomodel\VoiceInput(); // \Braango\braangomodel\VoiceInput | 

try {
    $result = $api_instance->createVoice($subdealerid, $salespersonid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoiceApi->createVoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **body** | [**\Braango\braangomodel\VoiceInput**](../Model/VoiceInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\VoiceOutput**](../Model/VoiceOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteOneVoice**
> \Braango\braangomodel\VoiceOutput deleteOneVoice($subdealerid, $salespersonid, $number, $apiKey, $accountType)

Delete One Voice

Delete one VOICE number associated with this personnel as specified in the URL

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\VoiceApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$number = "number_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteOneVoice($subdealerid, $salespersonid, $number, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoiceApi->deleteOneVoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **number** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\VoiceOutput**](../Model/VoiceOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVoice**
> \Braango\braangomodel\VoiceOutput deleteVoice($subdealerid, $salespersonid, $apiKey, $accountType)

Delete Voice

Delete all VOICE numbers associated with this personnel

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\VoiceApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteVoice($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoiceApi->deleteVoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\VoiceOutput**](../Model/VoiceOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVoice**
> \Braango\braangomodel\VoiceOutput getVoice($subdealerid, $salespersonid, $apiKey, $accountType)

Get Voice

Get VOICE numbers associated with this personnel

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\VoiceApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getVoice($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VoiceApi->getVoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\VoiceOutput**](../Model/VoiceOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

