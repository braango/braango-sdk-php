<?php
/**
 * SupervisorOutputPerGroupBodyDataTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Braango
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * braango
 *
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Braango;

/**
 * SupervisorOutputPerGroupBodyDataTest Class Doc Comment
 *
 * @category    Class */
// * @description Payload section of supervisor output for given group
/**
 * @package     Braango
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SupervisorOutputPerGroupBodyDataTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "SupervisorOutputPerGroupBodyData"
     */
    public function testSupervisorOutputPerGroupBodyData()
    {
    }

    /**
     * Test attribute "groupSupervisors"
     */
    public function testPropertyGroupSupervisors()
    {
    }
}
